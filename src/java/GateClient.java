
import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;


public class GateClient {
    
    private static SerialAnalyserController annotationPipeline = null;
    
    private static boolean isGateInitialised = false;
    
    public static ArrayList<String> run(InputStream file) throws MalformedURLException, IOException {
        ArrayList<String> list = new ArrayList<>();
        
        if (!isGateInitialised) {
            initialiseGate();
        }
        try {                
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            // create an instance of a Sentence Splitter processing resource
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            // create an instance of a Sentence Splitter processing resource
            ProcessingResource annieGazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
            
            // locate the JAPE grammar file
            File japeOrigFile = new File("/home/tomas/FIT/MI/MI-DDW/DDWukol1/jape.jape");
            java.net.URI japeURI = japeOrigFile.toURI();
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);
            
            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");
            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(annieGazetteer);
            annotationPipeline.add(japeTransducerPR);

           
            // create a document
            Document document = Factory.newDocument(IOUtils.toString(file, "UTF-8"));//new File("/home/tomas/FIT/MI/MI-DDW/document.txt").toURI().toURL()
            
            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);
            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);
            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for (int i = 0; i < corpus.size(); i++) {
                Document doc = corpus.get(i);
                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();
                FeatureMap featureMap = null;
                                
                // get all Telephones
                AnnotationSet annSetTel = as_default.get("Telephone",featureMap);
                ArrayList telAnnotations = new ArrayList(annSetTel);
                for (int j = 0; j < telAnnotations.size(); ++j) {
                    Annotation token = (Annotation) telAnnotations.get(j);
                    Node start = token.getStartNode();
                    Node end   = token.getEndNode();
                    String string = doc.getContent().getContent(start.getOffset(), end.getOffset()).toString();
                    list.add("Tel: " + string);
                }
                // get all Telephones
                AnnotationSet annSetEmail = as_default.get("Email",featureMap);
                ArrayList emailAnnotations = new ArrayList(annSetEmail);
                for (int j = 0; j < emailAnnotations.size(); ++j) {
                    Annotation token = (Annotation) emailAnnotations.get(j);
                    Node start = token.getStartNode();
                    Node end   = token.getEndNode();
                    String string = doc.getContent().getContent(start.getOffset(), end.getOffset()).toString();
                    list.add("Email: " + string);
                }

                
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return list;
    }

    private static void initialiseGate() {
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("/home/tomas/Aplikace/GATE_Developer_8.0");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("/home/tomas/Aplikace/GATE_Developer_8.0/plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("/home/tomas/Aplikace/GATE_Developer_8.0", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitialised = true;
            
            
        } catch (MalformedURLException | GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      
    
}
