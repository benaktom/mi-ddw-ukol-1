
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/result"})
@MultipartConfig
public class Servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
                
        String url = request.getParameter("url");
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Result | MI-DDW - homework 1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Result | MI-DDW - homework 1</h1>");
            out.println("Spambot - crawl content and extract emails or phone numbers (using JAPE grammar)");
            if (!url.contains("http://") && !url.contains("https://")) {
                out.println("<p>URL error. Please, write 'http://' or 'https://'</p>");
            }
            else {
                URL u = new URL(url);
                URLConnection con = u.openConnection();
                int code = 0;
                String message = "";
                String location = "";
                if (con instanceof HttpURLConnection) {
                    try {
                        HttpURLConnection hcon = (HttpURLConnection) con;
                        code = hcon.getResponseCode();
                        message = "<p>"+code+" "+hcon.getResponseMessage()+"</p>";
                        if (hcon.getHeaderField("Location") != null) {
                            location = "<p>Location: "+hcon.getHeaderField("Location")+"</p>";
                        }
                        hcon.disconnect();
                    }
                    catch (IOException e) {
                        out.println("<p>Bad URL.</p>");
                        out.println("<button onclick='window.history.back();'>Back</button>");
                        out.println("</body>");
                        out.println("</html>");
                    }
                }
                else {
                    message = "url error";
                }
                try {
                    InputStream is = u.openStream();
                    out.println(message);
                    out.println(location);
                    if (code == 200) {
                        ArrayList<String> list = GateClient.run(is);
                        if (!list.isEmpty()) {
                            out.println("<p>Results: "+list.size()+"</p><ul>");
                            for (String item : list) {
                                out.println("<li>"+item+"</li>");
                            }
                            out.println("</ul>");
                        }
                        else {
                            out.println("<p>Results: 0</p>");
                        }
                    }
                }
                catch (FileNotFoundException e) {
                    out.println("<p>"+e+"</p>");
                }
                catch (IOException e) {
                    out.println("<p>"+e+"</p>");
                }
            }
            out.println("<button onclick='window.history.back();'>Back</button>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
